/* Filename:    VideoConverter.cpp

   Description: This file conaints the interface for the VideoConverter class.
   The purpose of this class is to handle the conversion of an mp4 video to
   a data stream of RGB values. 
*/

#include "VideoConverter.h"
#include "WiringPiSetup.h"

using namespace std;
using namespace cv;
//////////////////////////// Constructor ////////////////////////////////////////////////////////////

VideoConverter::VideoConverter(Video video, PanelConfig panel_config)
{
    // Initialize vidoe and panel configuration
    set_video(video);
    set_panel_config(panel_config);
}

//////////////////////////// Getters and Setters ////////////////////////////////////////////////////

Video VideoConverter::video()                        { return _video; }
void VideoConverter::set_video(Video v)              { _video = v; }
PanelConfig VideoConverter::panel_config()           { return _panel_config; }
void VideoConverter::set_panel_config(PanelConfig p) { _panel_config = p; }

VideoState VideoConverter::video_state()             { return _video_state;}
void VideoConverter::set_video_state(VideoState s)                   { _video_state = s; }

//////////////////////////// Public Methods /////////////////////////////////////////////////////////


void VideoConverter::Convert(char mode)
{
    if(mode == 'l' || mode == 'c')
    {
      if (!setupWiringPi())
      {
        cout << "failed to setup wiringpi" << endl;
        return;
      }
    }

    if(mode == 'd' || mode == 'c' || mode == 'f')
    {
      outfile_fd = open("outfile.dd", O_WRONLY|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR);
      if(outfile_fd < 0 )
      {
        cout << "failed to open output file" << endl;
        return;
      }
    }

    set_video_state(PLAY);

    cv::Mat frame_source; 
    cv::Mat frame_cropped;
    cv::Mat frame_resized(cv::Size(_video.target_width, _video.target_height),CV_8U);
    
    cv::Rect  crop_region(_video.crop_left, _video.crop_top, _video.crop_width, _video.crop_height);

    if( mode == 'd' ) // Play video input and output in windows if on GUI
    {
        cv::namedWindow("input",1);
        cv::namedWindow("output",1);
    }

    // Setup and Open the File Stream
    cv::VideoCapture input_stream(_video.file_name);
    if(!input_stream.isOpened()) // check to see if file was opened succesfully
    {
        return;
    }
    
    while( input_stream.read( frame_source) )
    {
        set_video_state(PLAY);
        // Crop the frame
       // frame_cropped = frame_source(crop_region);
        // Resize the fream
        cv::Mat frame_cropRef(frame_source, crop_region);
        frame_cropRef.copyTo(frame_cropped);
        cv::resize(frame_cropped, frame_resized, frame_resized.size(), 0, 0, cv::INTER_AREA); 
        //  Apply Gamma Correction
        frame_resized = correctGamma(frame_resized, 0.25);

        if(mode == 'd' || mode == 'c' || mode == 'l')
          if(cv::waitKey(30) >= 0) break;

        if(mode == 'd') // Display input and output in windows if on Desktop OS
        {
            cv::imshow("input", frame_source);
            cv::imshow("output", frame_resized);
        } 

        if(mode == 'l')
          process_data(frame_resized, false, true);  // <--- most important function
        else if(mode == 'd' || mode == 'f')
          process_data(frame_resized, true, false);  // <--- most important function
        else if(mode == 'c')
          process_data(frame_resized, true, true);  // <--- most important function
            
    }
}

void VideoConverter::VideoInfo()
{
    printf("FileName: %s \n",  _video.file_name.c_str()) ;
    printf("Target Width: %d \t Target Height: %d \n", _video.target_width, _video.target_height);
    printf("Crop Left: %d    \t Crop Top: %d \n", _video.crop_left, _video.crop_top);
    printf("Crop Width: %d   \t Crop Height: %d \n", _video.crop_width, _video.crop_height);
}

//////////////////////////// Private Methods ////////////////////////////////////////////////////////


void VideoConverter::process_data(cv::Mat frame, bool saveit, bool showit)
{
    split_char tos;
    uint8_t * data = (uint8_t*)frame.data;
    int cn = frame.channels();

    int numLEDso2 = frame.rows * frame.cols * cn;

    if(saveit)
      write(outfile_fd, data, numLEDso2);

    numLEDso2 = numLEDso2 / 2;

    if(showit)
    {
      for(int i = 0; i < numLEDso2; i+=3)
      {
        tos.num = data[i + numLEDso2 + 0];
        write8bits(tos);
        tos.num = data[i + numLEDso2 + 1];
        write8bits(tos);
        tos.num = data[i + numLEDso2 + 2];
        write8bits(tos);

        tos.num = data[i + 0];
        write8bits(tos);
        tos.num = data[i + 1];
        write8bits(tos);
        tos.num = data[i + 2];
        write8bits(tos);
      }
    }
}

cv::Mat VideoConverter::correctGamma( cv::Mat& img, double gamma ) {
    // Function From:
    //  http://subokita.com/2013/06/18/simple-and-fast-gamma-correction-on-opencv/using namespace cv;
    
    //double inverse_gamma = 1.0 / gamma;
    cv::Mat lut_matrix(1, 256, CV_8UC1 );
    uchar * ptr = lut_matrix.ptr();
    double L;
    for( int i = 0; i < 256; i++ )
    {
      L = (double)i/(double)255;
      L *= 100;
      if (L <= 8)
        L = L/902.3;
      else
        L = ((L+16)/(double)116)*((L+16)/(double)116)*((L+16)/(double)116);
      ptr[i] = int(L*255);
      //ptr[i] = (int)( pow( (double) i / 255.0, inverse_gamma ) * 255.0 );
    }

    cv::Mat result;
    LUT( img, lut_matrix, result );

    return result;
}

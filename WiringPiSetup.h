#include<stdio.h>
#include<wiringPi.h>
#include<time.h>
#include<iostream>
#include<stdlib.h>
#include<fcntl.h>

using namespace std;

  typedef union 
  {
    unsigned char num;
    struct {
      unsigned int b0: 1;
      unsigned int b1: 1;
      unsigned int b2: 1;
      unsigned int b3: 1;
      unsigned int b4: 1;
      unsigned int b5: 1;
      unsigned int b6: 1;
      unsigned int b7: 1;
    } bits;
  } split_char;

int setupWiringPi()
{
  if (wiringPiSetup()  == -1 )
  {
    cout << "failed to set up wiring pi" << endl;
    exit(-1);
  }

  pinMode(0, OUTPUT); //GPIO 17
  pinMode(1, OUTPUT); //GPIO 17
  pinMode(2, OUTPUT); //GPIO 17
  pinMode(3, OUTPUT); //GPIO 17
  pinMode(4, OUTPUT); //GPIO 17
  pinMode(5, OUTPUT); //GPIO 17
  pinMode(6, OUTPUT); //GPIO 16
  pinMode(7, OUTPUT); //GPIO 17
  pinMode(8, OUTPUT); //GPIO 17
  pinMode(9, INPUT);
  pinMode(10, OUTPUT);

  digitalWrite(8, 0);
  digitalWrite(10, 1);
  digitalWrite(10, 0);
}

void write8bits(split_char tosend)
{
  digitalWrite(0, tosend.bits.b0);
  digitalWrite(1, tosend.bits.b1);
  digitalWrite(2, tosend.bits.b2);
  digitalWrite(3, tosend.bits.b3);
  digitalWrite(4, tosend.bits.b4);
  digitalWrite(5, tosend.bits.b5);
  digitalWrite(6, tosend.bits.b6);
  digitalWrite(7, tosend.bits.b7);
  digitalWrite(8, 1);
  digitalWrite(8, 0);
}


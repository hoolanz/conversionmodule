/* Filename:    VideoConverter.h

   Description: This file conaints the interface for the VideoConverter class.
   The purpose of this class is to handle the conversion of an mp4 video to
   a data stream of RGB values. 
*/

#ifndef VIDEOCONVERTERH
#define VIDEOCONVERTERH

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// Stuct to hold video params. Passed to VideoConverter Constructor
struct Video {
    std::string file_name; 
    int target_width;
    int target_height;
    int crop_left;
    int crop_top;
    int crop_width;
    int crop_height;
}typedef Video;

// Stuct to hold panel config params. Passed to VideoConverter Constructor
struct PanelConfig{
    int pixels_w;   // Number of pixels wide (Ada fruit are 32)
    int pixels_h;   // Number if pixels tall (32 or 16 Typical)
    int count;      // Number of LED Panels
    int rows;       // Number of Rows of Panels ( Vertical)
    int cols;       // Number of Cols of Panels ( Horizontal)
}typedef PanelConfig;
enum VideoState {PLAY, PAUSE, STOP};

class VideoConverter{

    public:
        // Constructor
        VideoConverter(Video video, PanelConfig panel_config);

        // Accessors and Mutators
        Video video();
        void set_video(Video v);

        VideoState video_state();
        void set_video_state(VideoState s);

        PanelConfig panel_config();
        void set_panel_config(PanelConfig p);

        // Public Methods
        void VideoInfo(); // prints out details about video and panel configuration
        void Convert(char mode); // Starts conversion/output. Plays videos if show_video is true
    private:
        // Private fields
        int outfile_fd;
        Video _video;      
        PanelConfig _panel_config;
        VideoState _video_state; 
        // Private methods
        void process_data(cv::Mat frame, bool, bool); // converts video to data matrix. handles output
        cv::Mat correctGamma(cv::Mat& img, double gamma ); // performs gamma correction
};

#endif

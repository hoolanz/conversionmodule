/* Filename:    ConversionModule.cpp

   Description: This file contains the main function for the ConversionModule.
                It accepts a series of command line arguments describing an input
                video file and then creates a new VideoConverter to convert the 
                video
*/

#include <iostream>
#include "VideoConverter.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;


int main(int argc, char* argv[])
{

   if(argc >= 8 )     
   {
        Video v = {
                    argv[1], // Video File Name
                    atoi(argv[2]), // Target width in pixels
                    atoi(argv[3]), // Target height in pixels
                    atoi(argv[4]), // Crop Left 
                    atoi(argv[5]), // Crop Top 
                    atoi(argv[6]), // Crop Width 
                    atoi(argv[7]), // Crop Height 
        };
        PanelConfig p = { 32, 32, 2, 1, 2 };
        VideoConverter vc(v, p);
        vc.VideoInfo();
        if(argc > 8)
        {
            switch(argv[8][0])
            {
              case 'd':       // display on pc and create file
                vc.Convert('d');
                break;
              case 'c':        // display on leds and create file
                vc.Convert('c');
                break;
              case 'l':        // display on leds
                vc.Convert('l');
                break;
              default:
                vc.Convert('f');
                break;
            }
        }
        else
        {
            vc.Convert('f');
        }
    }
    else
    {
        printf("\n Usage: ConversionModule 'filename.mp4' out_w out_h crop_w crop_h crop_t crop_l \n\n");
    }
}


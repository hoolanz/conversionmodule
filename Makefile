# Makefile for Conversion Module

CFLAGS = `pkg-config --libs opencv --cflags opencv`

all : VideoConverter 
VideoConverter:  VideoConverter.cpp VideoConverter.h ConversionModule.cpp
	g++ -o ConversionModule ConversionModule.cpp VideoConverter.cpp  $(CFLAGS) -lwiringPi 
	
clean:
	rm -f *.o ConversionModule 

